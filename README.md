# rcl2csv: an RCL parser for the RoboCup Soccer Simulation 2D League

*rcl2csv* is a pure C++20 (no dependecies) transpiler of RoboCup Command Logs (RCL) to CSV data tables.

It is designed to be simple and single-purpose. Combine it with other CLI utilities to build useful data pipelines.

## Notes

- Doesn't currently support Offline Coach, Online Coach or Monitor commands. Use it to extract player-issued commands.
- Doesn't support *rcssserver* profiling messages (don't use with matches ran with `server::profile=on`)
