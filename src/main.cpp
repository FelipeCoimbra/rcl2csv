#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>

#include "parser.hpp"

class SampleEmitter : public RCLParsingListener {
public:
    virtual void onRCLEntry(size_t lineNumber, const std::string& completeLine) noexcept {
        std::cout << "On RCLEntry: " << lineNumber << " \"" << completeLine << "\"" << std::endl;
    }
    virtual void onCommandTime(CommandTime&& time) noexcept  {
        std::cout << "On CommandTime: " << time.runningStep << " " << time.stoppedStep << " " << time.globalCommandCount << std::endl;
    }
    virtual void onPlayerClient(std::string&& teamName, int uniformNumber) noexcept  {
        std::cout << "On PlayerClient: " << teamName << " " << uniformNumber << std::endl;
    }
    virtual void onOnlineCoachClient(std::string&& teamName) noexcept  {
        std::cout << "On OnlineCoachClient: " << teamName << std::endl;
    }
    virtual void onOfflineCoachClient(std::string&&teamName) noexcept  {
        std::cout << "On OfflineCoachClient: " << teamName << std::endl;
    }
    virtual void onPlayerInitCommand(std::string teamName, bool isGoalie, double version) noexcept {
        std::cout << "On PlayerInitCommand: " << teamName << " " << isGoalie << " " << version << std::endl;
    }
    virtual void onPlayerReconnectCommand(std::string teamName, int uniformNumber) noexcept {
        std::cout << "On PlayerReconnectCommand: " << teamName << " " << uniformNumber << std::endl;
    }
    virtual void onDashCommand(double power, double direction) noexcept {
        std::cout << "On DashCommand: " << power << " " << direction << std::endl;
    }
    virtual void onTurnCommand(double moment) noexcept {
        std::cout << "On TurnCommand: " << moment << std::endl;
    }
    virtual void onKickCommand(double power, double direction) noexcept {
        std::cout << "On KickCommand: " << power << " " << direction << std::endl;
    }
    virtual void onTackleCommand(double direction, bool foul) noexcept {
        std::cout << "On TackleCommand: " << direction << " " << foul << std::endl;
    }
    virtual void onCatchCommand(double direction) noexcept {
        std::cout << "On CatchCommand: " << direction << std::endl;
    }
    virtual void onTurnNeckCommand(double moment) noexcept {
        std::cout << "On TurnNeckCommand: " << moment << std::endl;
    }
    virtual void onChangeViewCommand(ViewWidth viewWidth, ViewQuality viewQuality) noexcept {
        std::cout << "On ChangeViewCommand: " << viewWidth << " " << viewQuality << std::endl;
    }
    virtual void onAttentionToCommand(AttentionToCommand attentionTo) noexcept {
        std::cout << "On AttentionToCommand: ";
        if (!attentionTo.isTurnedOn()) {
            std::cout << "Off";
        } else {
            std::cout << "On " << attentionTo.getTarget().value().side << " " << attentionTo.getTarget().value().uniformNumber;
        }
        std::cout << std::endl;
    }
    virtual void onPointToCommand(PointToCommand pointTo) noexcept {
        std::cout << "On PointToCommand: ";
        if (!pointTo.isTurnedOn()) {
            std::cout << "Off";
        } else {
            std::cout << "On " << pointTo.getTarget().value().distance << " " << pointTo.getTarget().value().direction;
        }
        std::cout << std::endl;
    }
    virtual void onEarCommand(EarCommand ear) noexcept {
        std::cout << "On EarCommand: " << ear.getAction();
        if (ear.hasParams()) {
            std::cout << " ";
            auto params = ear.getParams().value();
            if (params.hasTeamNameTarget()) {
                std::cout << params.getTeamName().value();
            } else {
                std::cout << params.getEnumTarget().value();
            }
            std::cout << " " << params.getType();
        }
        std::cout << std::endl;
    }
    virtual void onSayCommand(std::string sayMessage) noexcept {
        std::cout << "On SayCommand: " << sayMessage << std::endl;
    }
    virtual void onMoveCommand(double x, double y) noexcept {
        std::cout << "On MoveCommand: " << x << " " << y << std::endl;
    }
    virtual void onScoreCommand() noexcept {
        std::cout << "On ScoreCommand." << std::endl;
    }
    virtual void onClangCommand(int clangMinVersion, int clangMaxVersion) noexcept {
        std::cout << "On ClangCommand: " << clangMinVersion << " " << clangMaxVersion << std::endl;
    }
    virtual void onSynchSeeCommand() noexcept {
        std::cout << "On SynchSeeCommand." << std::endl;
    }
    virtual void onByeCommand() noexcept {
        std::cout << "On ByeCommand." << std::endl;
    }
    virtual void onCompressionCommand(int compressionLevel) noexcept {
        std::cout << "On CompressionCommand: " << compressionLevel << std::endl;
    }
    virtual void onDoneCommand() noexcept {
        std::cout << "On DoneCommand." << std::endl;
    }
};

class RCL2CSVEmitter : public RCLParsingListener {
public:
    explicit
    RCL2CSVEmitter(std::filesystem::path outputDirPath)
    : outputDirPath(std::move(outputDirPath))
    {
        tableNamePrefix = "matchcommands";
        openTableOutputs();
        printTableHeaders();
        resetEmissionState();
    }

    RCL2CSVEmitter(std::filesystem::path outputDirPath, std::string tableNamePrefix)
    : outputDirPath(std::move(outputDirPath))
    , tableNamePrefix(std::move(tableNamePrefix))
    {
        openTableOutputs();
        printTableHeaders();
        resetEmissionState();
    }

    ~RCL2CSVEmitter() {
        dashTableStream.close();
        turnTableStream.close();
        kickTableStream.close();
        tackleTableStream.close();
    }

    virtual void onCommandTime(CommandTime&& time) noexcept  {
        runningTime = time.runningStep;
        stoppedTime = time.stoppedStep;
        globalCommandCount = time.globalCommandCount;
    }
    virtual void onPlayerClient(std::string&& teamName, int uniformNumber) noexcept  {
        playerTeamName = teamName;
        this->uniformNumber = uniformNumber;
    }
    virtual void onDashCommand(double power, double direction) noexcept {
        dashTableStream << ++totalDashCount 
                        << "," << runningTime
                        << "," << stoppedTime
                        << "," << globalCommandCount
                        << "," << playerTeamName
                        << "," << uniformNumber
                        << "," << power
                        << "," << direction
                        << std::endl;
    }
    virtual void onTurnCommand(double moment) noexcept {
        turnTableStream << ++totalTurnCount 
                        << "," << runningTime
                        << "," << stoppedTime
                        << "," << globalCommandCount
                        << "," << playerTeamName
                        << "," << uniformNumber
                        << "," << moment
                        << std::endl;
    }
    virtual void onKickCommand(double power, double direction) noexcept {
        kickTableStream << ++totalKickCount 
                        << "," << runningTime
                        << "," << stoppedTime
                        << "," << globalCommandCount
                        << "," << playerTeamName
                        << "," << uniformNumber
                        << "," << power
                        << "," << direction
                        << std::endl;
    }
    virtual void onTackleCommand(double direction, bool foul) noexcept {
        tackleTableStream   << ++totalTackleCount 
                            << "," << runningTime
                            << "," << stoppedTime
                            << "," << globalCommandCount
                            << "," << playerTeamName
                            << "," << uniformNumber
                            << "," << direction
                            << "," << foul
                            << std::endl;
    }


private:
    static constexpr const char* DASH_TABLE_FILENAME_SUFFIX = ".dash.csv";
    static constexpr const char* TURN_TABLE_FILENAME_SUFFIX = ".turn.csv";
    static constexpr const char* KICK_TABLE_FILENAME_SUFFIX = ".kick.csv";
    static constexpr const char* TACKLE_TABLE_FILENAME_SUFFIX = ".tackle.csv";

    void openTableOutputs() {
        //
        // Dash
        //
        auto dashTablePath = std::filesystem::absolute(outputDirPath / (tableNamePrefix + DASH_TABLE_FILENAME_SUFFIX));
        dashTableStream.open(dashTablePath.string());
        if (dashTableStream.fail()) {
            std::stringstream ss;
            ss << "Failed to open " << dashTablePath.string() << std::endl;
            throw std::runtime_error(ss.str());
        }
        //
        // Turn
        //
        auto turnTablePath = std::filesystem::absolute(outputDirPath / (tableNamePrefix + TURN_TABLE_FILENAME_SUFFIX));
        turnTableStream.open(turnTablePath.string());
        if (turnTableStream.fail()) {
            std::stringstream ss;
            ss << "Failed to open " << turnTablePath.string() << std::endl;
            throw std::runtime_error(ss.str());
        }
        //
        // Kick
        //
        auto kickTablePath = std::filesystem::absolute(outputDirPath / (tableNamePrefix + KICK_TABLE_FILENAME_SUFFIX));
        kickTableStream.open(kickTablePath.string());
        if (kickTableStream.fail()) {
            std::stringstream ss;
            ss << "Failed to open " << kickTablePath.string() << std::endl;
            throw std::runtime_error(ss.str());
        }
        //
        // Tackle
        //
        auto tackleTablePath = std::filesystem::absolute(outputDirPath / (tableNamePrefix + TACKLE_TABLE_FILENAME_SUFFIX));
        tackleTableStream.open(tackleTablePath.string());
        if (dashTableStream.fail()) {
            std::stringstream ss;
            ss << "Failed to open " << tackleTablePath.string() << std::endl;
            throw std::runtime_error(ss.str());
        }
    }

    void printTableHeaders() noexcept {
        dashTableStream << "#,running_time,stopped_time,global_command_order,teamname,unum,dash_power,dash_direction" << std::endl;
        turnTableStream << "#,running_time,stopped_time,global_command_order,teamname,unum,turn_moment" << std::endl;
        kickTableStream << "#,running_time,stopped_time,global_command_order,teamname,unum,kick_power,kick_direction" << std::endl;
        tackleTableStream << "#,running_time,stopped_time,global_command_order,teamname,unum,tackle_direction,foul_intention" << std::endl;
    }

    void resetEmissionState() noexcept {
        runningTime = -1;
        stoppedTime = -1;
        globalCommandCount = 0;
        uniformNumber = -1;
        totalDashCount = 0;
        totalTurnCount = 0;
        totalKickCount = 0;
        totalTackleCount = 0;
    }

    //
    // Output objects
    //
    std::filesystem::path outputDirPath;
    std::string tableNamePrefix;
    std::ofstream dashTableStream;
    std::ofstream turnTableStream;
    std::ofstream kickTableStream;
    std::ofstream tackleTableStream;

    //
    // Emission state
    //
    int runningTime;
    int stoppedTime;
    size_t globalCommandCount;
    std::string playerTeamName;
    int uniformNumber;
    size_t totalDashCount;
    size_t totalTurnCount;
    size_t totalKickCount;
    size_t totalTackleCount;
};

void usage(std::ostream& os) {
    os << "Usage: rcl2csv <Output Folder> [<Output Tables Prefix>]" << std::endl;
    os << "\tReads RoboCup Command Log (RCL) entries from stdin and generates multiple CSV tables (1 per command type)." << std::endl;
    os << "\tExample: \"mkdir -p ./outdir && cat \"test\" | rcl2csv ./outdir/ coolprefixname\"" << std::endl;
}

int main(int argc, char **argv) {
    if (argc < 2 || argc > 3) {
        std::cerr << "ERROR: Wrong number of arguments. Expecting 1 or 2, got " << argc-1 << "." << std::endl;
        usage(std::cerr);
        exit(1);
    }
    auto listener = argc == 2 
        ? std::unique_ptr<RCLParsingListener>(new RCL2CSVEmitter(argv[1])) 
        : std::unique_ptr<RCLParsingListener>(new RCL2CSVEmitter(argv[1], argv[2]))
        ;
    RCLParser parser;
    parser.setListener(std::move(listener));
    // parser.setListener(std::unique_ptr<RCLParsingListener>(new SampleEmitter()));
    parser.parse(std::cin);
    return 0;
}
