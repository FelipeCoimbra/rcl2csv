#include <charconv>
#include <vector>
#include <sstream>

#include "parser.hpp"

void
RCLParser::parse(std::istream& logStreamer) noexcept {
    std::string line;

    // Auxiliary variables
    bool parseSuccessful;
    // Reset internal variables
    lineCount = 0;
    timeTracker = CommandTimeTracker();
    
    while (!logStreamer.eof()) {
        //
        // Read complete line
        //
        std::getline(logStreamer, line);
        
        if (logStreamer.bad() || (logStreamer.fail() && !logStreamer.eof())) {
            std::cerr << logStreamer.eof() << std::endl;
            std::cerr << "ERROR [Line " << lineCount+1 << "]: Unexpected read error while processing log entry." << std::endl;
            break;
        }
        ++lineCount;
        if (line.length() == 0) {
            std::cerr << "ERROR [Line " << lineCount << "]: Empty line. Skip." << std::endl;
            continue;
        }

        //
        // Call listener
        //
        if (listener) listener->onRCLEntry(lineCount, line);

        size_t readOffset = 0;
        std::optional<std::string_view> token;
        
        //
        // Command issued time
        //
        token = extractTime(line, readOffset);
        if (!token) {
            std::cerr << "ERROR [Line " << lineCount << "]: Failed to extract Command Time token." << std::endl;
            continue;
        }
        parseSuccessful = parseTime(token.value());
        if (!parseSuccessful) {
            std::cerr << "ERROR [Line " << lineCount << "]: Failed to parse Command Time." << std::endl;
            continue;
        }

        //
        // Whitespace
        //
        while (readOffset < line.length() && std::isspace(line[readOffset])) readOffset++;
        //
        // Referee or RECV
        //
        if (line.compare(readOffset, 9, "(referee ") == 0) {
            // TODO: Add support for Referee messages
            continue;
        }
        //
        // RECV token (Command Type) + leading whitespace
        // TODO: Allow for PROFILE entries too.
        //
        if (line.compare(readOffset, 5, "Recv ") != 0) {
            std::cerr << "ERROR [Line " << lineCount << "]: \"" << line.substr(readOffset, 5) << "\" mismatches \"Recv \"" << std::endl;
            std::cerr << "ERROR [Line " << lineCount << "]: Failed to parse Command Type." << std::endl;
            continue;
        }
        readOffset += 5;

        //
        // Whitespace
        //
        while (readOffset < line.length() && std::isspace(line[readOffset])) readOffset++;
        //
        // Client issuer
        //
        token = extractClient(line, readOffset);
        if (!token.has_value()) {
            std::cerr << "ERROR [Line " << lineCount << "]: Failed to extract Client token." << std::endl;
            continue;
        }
        parseSuccessful = parseClient(token.value());
        if (!parseSuccessful) {
            std::cerr << "ERROR [Line " << lineCount << "]: Failed to parse Client." << std::endl;
            continue;
        }

        //
        // Whitespace
        //
        while (readOffset < line.length() && std::isspace(line[readOffset])) readOffset++;

        //
        // Branch to dedicated client parser
        //
        switch (lastTypeParsed)
        {
        case ClientType::Player:
            if (!parsePlayerCommands(line, readOffset)) {
                std::cerr << "ERROR [Line " << lineCount << "]: Failed to parse player commands." << std::endl;
            }
            break;
        case ClientType::OnlineCoach:
            // TODO: Parse OnlineCoach Commands
            break;
        case ClientType::OfflineCoach:
            // TODO: Parse OfflineCoach Commands
            break;
        default:
            std::cerr << "ERROR [Line " << lineCount << "]: No available dedicated parser for " << lastTypeParsed << "." << std::endl;
            continue;
        }
    }
}

std::optional<std::string_view>
RCLParser::extractTime(const std::string& rclLine, size_t& offset) noexcept {
    size_t start = offset;
    while (offset < rclLine.length() && !std::isspace(rclLine[offset])) offset++;
    if (offset == start || offset == rclLine.length()) {
        std::cerr << "ERROR [Line " << lineCount << "]: No Time token extracted. Expected \"<running time>,<time>\"" << std::endl;
        return {};
    }
    return {std::string_view(rclLine.data()+start, offset-start)};
}

bool
RCLParser::parseTime(const std::string_view& timeDescription) noexcept {
    // Format: <running time>,<stopped time>
    auto commaPosition = timeDescription.find_first_of(',');
    if (commaPosition == std::string::npos || commaPosition == timeDescription.length()-1) {
        std::cerr << "ERROR: Invalid CommandTime token \"" << timeDescription << "\"" << std::endl;
        return false;
    }
    int runningTime, stoppedTime;
    //
    // Running time
    //
    auto result = std::from_chars(timeDescription.data(), timeDescription.data()+commaPosition, runningTime);
    if (result.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Could not convert \""<< timeDescription.substr(0,commaPosition) << "\" into int." << std::endl;
        return false;
    }
    if (result.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse running time from token \"" << timeDescription << "\"." << std::endl;
        std::cerr << "\t" << timeDescription.substr(0,commaPosition) << " is out of int range." << std::endl;
        return false;
    }
    if (result.ptr != timeDescription.data()+commaPosition) {
        std::cerr << "ERROR: Invalid CommandTime token \"" << timeDescription << "\"." << std::endl;
        std::cerr << "\tLeft trailing content after parsing Running Time: \"" << timeDescription.substr(0, result.ptr-timeDescription.data()) << "\"." << std::endl;
        return false;
    }
    //
    // Stopped time
    //
    result = std::from_chars(timeDescription.data()+commaPosition+1, timeDescription.data()+timeDescription.length(), stoppedTime);
    if (result.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Failed to parse stopped time from token \"" << timeDescription << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< timeDescription.substr(commaPosition+1) << "\" into int." << std::endl;
        return false;
    }
    if (result.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse stopped time from token \"" << timeDescription << "\"." << std::endl;
        std::cerr << "\t\"" << timeDescription.substr(commaPosition+1) << "\" is out of int range." << std::endl;
        return false;
    }
    if (result.ptr != timeDescription.data() + timeDescription.length()) {
        std::cerr << "ERROR: Invalid CommandTime token \"" << timeDescription << "\"." << std::endl;
        std::cerr << "\tLeft trailing content after parse: \"" << timeDescription.substr(result.ptr-timeDescription.data()) << "\"" << std::endl;
        return false;
    }
    auto commandTime = timeTracker.nextTime(runningTime, stoppedTime);
    
    //
    // Call listener
    //
    if (listener) listener->onCommandTime(std::move(commandTime));

    return true;
}

std::optional<std::string_view> 
RCLParser::extractClient(const std::string& rclLine, size_t& offset) noexcept {
    size_t start = offset;
    while (offset < rclLine.length() && rclLine[offset] != ':') offset++;
    if (offset == start || offset == rclLine.length()) {
        std::cerr << "ERROR [Line " << lineCount << "]: No Client extracted. Expected \"<client specification>:\"" << std::endl;
        return {};
    }
    return {std::string_view(rclLine.data()+start, offset++ - start)};
}

bool
RCLParser::parseClient(const std::string_view& clientDescription) noexcept {
    ClientType type;
    std::string_view teamName;
    int playerUniform;
    if (clientDescription.ends_with("Coach")) {
        //
        // Online Coach Client or Offline Coach Client
        // Online format: <team name>_Coach
        // Offline format: <team name> Coach
        //
        const size_t coachSuffixSize = std::strlen("*Coach"); // '*' may be '_' or ' '.
        try {
            char coachSeparator = clientDescription.at(clientDescription.length()-coachSuffixSize);
            switch (coachSeparator) {
                case '_':
                    type = ClientType::OnlineCoach;
                    break;
                case ' ':
                    type = ClientType::OfflineCoach;
                    break;
                default:
                    std::cerr << "ERROR: Invalid CoachClient token \"" << clientDescription << "\". " << std::endl;
                    std::cerr << "\tInvalid coach separator \"" << coachSeparator << "\"" << std::endl;
                    return false;
            }
        } catch (std::out_of_range& err) {
            std::cerr << "ERROR: Invalid CoachClient token \"" << clientDescription << "\". " << std::endl;
            std::cerr << "\tNo coach separator." << std::endl;
            return false;
        }
        teamName = clientDescription.substr(0, clientDescription.length()-coachSuffixSize);
    } else {
        //
        // Player Client
        // FORMAT: <team name>_<unum>
        //
        type = ClientType::Player;
        size_t unumSeparatorPosition = clientDescription.find_last_of('_');
        if (unumSeparatorPosition == std::string::npos) {
            std::cerr << "ERROR: Invalid PlayerClient token \"" << clientDescription << "\"." << std::endl;
            std::cerr << "\tNo Uniform Number separator." << std::endl;
            return false;
        }
        if (unumSeparatorPosition == clientDescription.length()-1) {
            std::cerr << "ERROR: Invalid PlayerClient token \"" << clientDescription << "\"." << std::endl;
            std::cerr << "\tNo Uniform Number." << std::endl;
            return false;
        }
        teamName = clientDescription.substr(0,unumSeparatorPosition);
        auto unumResult = std::from_chars(clientDescription.data()+unumSeparatorPosition+1, clientDescription.data()+clientDescription.length(), playerUniform);
        if (unumResult.ec == std::errc::invalid_argument) {
            std::cerr << "ERROR: Failed to parse uniform number from token \"" << clientDescription << "\"." << std::endl;
            std::cerr << "\tCould not convert \""<< clientDescription.substr(unumSeparatorPosition+1) << "\" into int." << std::endl;
            return false;
        }
        if (unumResult.ec == std::errc::result_out_of_range) {
            std::cerr << "ERROR: Failed to parse uniform number from token \"" << clientDescription << "\"." << std::endl;
            std::cerr << "\t" << clientDescription.substr(unumSeparatorPosition+1) << " is out of int range." << std::endl;
            return false;
        }
        if (unumResult.ptr != clientDescription.data() + clientDescription.length()) {
            std::cerr << "ERROR: Invalid PlayerClient token \"" << clientDescription << "\"." << std::endl;
            std::cerr << "\tLeft trailing content after parse: \"" << clientDescription.substr(unumResult.ptr-clientDescription.data()) << "\"" << std::endl;
        }
    }

    if (teamName.empty()) {
        std::cerr << "ERROR: Invalid Client token \"" << clientDescription << "\"." << std::endl;
        std::cerr << "\tNo Team Name." << std::endl;
        return false;
    }

    //
    // A few semantic checks
    //
    constexpr const char* TEAMNAME_VALID_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+";
    constexpr const size_t TEAMNAME_MAX_LENGTH = 15;
    if (teamName.find_first_not_of(TEAMNAME_VALID_CHARS) != std::string::npos) {
        std::cerr << "ERROR: Invalid Client token \"" << clientDescription << "\"." << std::endl;
        std::cerr << "\tInvalid Team Name." << std::endl;
        return false;
    }
    if (teamName.length() > TEAMNAME_MAX_LENGTH) {
        std::cerr << "ERROR: Invalid Client token \"" << clientDescription << "\"." << std::endl;
        std::cerr << "\tExceeds maximum Team Name length (15)." << std::endl;
        return false;
    }
    if (type == ClientType::Player && (playerUniform <= 0 || playerUniform >= 12)) {
        std::cerr << "ERROR: Invalid PlayerClient token \"" << clientDescription << "\"." << std::endl;
        std::cerr << "\tInvalid Uniform Number (outside range [1,11])." << std::endl;
        return false;
    }

    //
    // Call listener
    //
    switch (type)
    {
    case ClientType::Player:
        if (listener) listener->onPlayerClient({teamName.begin(), teamName.end()}, playerUniform);
        break;
    case ClientType::OnlineCoach:
        if (listener) listener->onOnlineCoachClient({teamName.begin(), teamName.end()});
        break;
    case ClientType::OfflineCoach:
        if (listener) listener->onOfflineCoachClient({teamName.begin(), teamName.end()});
        break;
    default:
        std::cerr << "Unsupported client type " << std::endl;
        return false;
    }
    lastTypeParsed = type;

    return true;
}

bool
RCLParser::parsePlayerCommands(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
        std::cerr << "\tNo player commands." << std::endl;
        return false;
    }
    std::uint32_t parsedPlayerCommands = 0;
    size_t commandStart;
    bool parseResult;
    //
    // Extract all player commands sequentially
    //
    while (offset < rclLine.length()) {
        //
        // Collect command start
        //
        size_t openScopePosition = rclLineView.find_first_of('(', offset);
        if (openScopePosition == std::string_view::npos) {
            if (parsedPlayerCommands == 0) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tNo player commands." << std::endl;
                return false;
            } else {
                std::cerr << "WARNING: Found trailing junk in PlayerCommand list \"" << rclLineView.substr(offset) << "\"." << std::endl;
                std::cerr << "\tSkipping \"" << rclLineView.substr(offset) << "\"." << std::endl;
                offset = rclLineView.length();
                continue;
            }
        } else if (openScopePosition > offset) {
            auto leadingJunkView = rclLineView.substr(offset, openScopePosition-offset);
            std::cerr << "WARNING: Found junk before PlayerCommand start \"" << leadingJunkView << "\"." << std::endl;
            std::cerr << "\tSkipping \"" << leadingJunkView << "\"." <<std::endl;
            offset = openScopePosition;
        }
        //
        // Recognize command
        //
        commandStart = offset;
        if (rclLineView.compare(offset, 6, "(init ") == 0) {
            //
            // Init
            //
            offset += 5;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parsePlayerInit(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid init comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 11, "(reconnect ") == 0) {
            //
            // Reconnect
            //
            offset += 10;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseReconnect(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid reconnect comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 6, "(dash ") == 0) {
            //
            // Dash
            //
            offset += 5;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseDash(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid dash comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 6, "(turn ") == 0) {
            //
            // Turn
            //
            offset += 5;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseTurn(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid turn comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 6, "(kick ") == 0) {
            //
            // Kick
            //
            offset += 5;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseKick(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid kick comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 8, "(tackle ") == 0) {
            //
            // Tackle
            //
            offset += 7;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseTackle(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid tackle comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 7, "(catch ") == 0) {
            //
            // Catch
            //
            offset += 6;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseCatch(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid catch comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 11, "(turn_neck ") == 0) {
            //
            // TurnNeck
            //
            offset += 10;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseTurnNeck(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid turn_neck comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 13, "(change_view ") == 0) {
            //
            // ChangeView
            //
            offset += 12;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseChangeView(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid change_view comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 13, "(attentionto ") == 0) {
            //
            // AttentionTo
            //
            offset += 12;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseAttentionTo(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid attentionto comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 9, "(pointto ") == 0) {
            //
            // PointTo
            //
            offset += 8;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parsePointTo(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid pointto comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 5, "(say ") == 0) {
            //
            // Say
            //
            offset += 4;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseSay(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid say comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 5, "(ear ") == 0) {
            //
            // Ear
            //
            offset += 4;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseEar(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid ear comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 6, "(move ") == 0) {
            //
            // Move
            //
            offset += 5;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseMove(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid move comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 7, "(score)") == 0) {
            //
            // Score
            //
            if (listener) listener->onScoreCommand();
            offset += 7;
        } else if (rclLineView.compare(offset, 7, "(clang ") == 0) {
            //
            // Clang
            //
            offset += 6;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseClang(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid clang comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 11, "(synch_see)") == 0) {
            //
            // SynchSee
            //
            if (listener) listener->onSynchSeeCommand();
            offset += 11;
        } else if (rclLineView.compare(offset, 5, "(bye)") == 0) {
            //
            // Bye
            //
            if (listener) listener->onByeCommand();
            offset += 5;
        } else if (rclLineView.compare(offset, 13, "(compression ") == 0) {
            //
            // Compression
            //
            offset += 12;
            //
            // Whitespace
            //
            while (offset < rclLineView.length() && std::isspace(rclLineView[offset])) offset++;
            //
            // Command
            //
            parseResult = parseCompression(rclLine, offset);
            if (!parseResult) {
                std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
                std::cerr << "\tInvalid compression comand \"" << rclLineView.substr(commandStart) << "\"." << std::endl;
                return false;
            }
        } else if (rclLineView.compare(offset, 6, "(done)") == 0) {
            //
            // Done
            //
            if (listener) listener->onDoneCommand();
            offset += 6;
        } else {
            //
            // Unrecognized
            //
            std::cerr << "ERROR: Invalid PlayerCommands." << std::endl;
            std::cerr << "\tUnrecognized command \"" << rclLineView.substr(offset) << "\"." << std::endl;
            return false;
        }

        // Next command
        parsedPlayerCommands++;
    }

    return true;
}

bool
RCLParser::parsePlayerInit(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t initStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing team name." << std::endl;
        return false;
    }

    //
    // Team Name
    //
    size_t teamNameStart = offset;
    while (offset < rclLine.length() && !std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing team name/goalie or version separator." << std::endl;
        return false;
    }
    std::string teamName = rclLine.substr(teamNameStart, offset-teamNameStart);
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing goalie or version." << std::endl;
        return false;
    }
    //
    // Goalie [Optional]
    //
    bool goalie = false;
    if (rclLine.compare(offset, 8, "(goalie)") == 0) {
        goalie = true;
        offset += 8;
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
            std::cerr << "\tMissing version separator." << std::endl;
            return false;
        }
    }
    //
    // Version scope
    //
    if (rclLineView.compare(offset, 9, "(version ") != 0) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        if (!goalie) std::cerr << "\tNo (goalie) token." << std::endl;
        std::cerr << "\tMissing \"(version \" token." << std::endl;
        return false;
    }
    offset += 8; // Skip "(version"
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing team name/goalie or version separator." << std::endl;
        return false;
    }
    //
    // Version number
    //
    size_t versionNumberStart = offset;
    offset = rclLine.find(')', versionNumberStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing version scope end \")\"." << std::endl;
        return false;
    }
    errno = 0;
    char* ptr = nullptr;
    // Version shouldn't really be stored as a floating point, but that's how the rcssserver treats it
    double versionNumber = std::strtod(rclLine.data()+versionNumberStart, &ptr);
    if (ptr == rclLineView.data()+versionNumberStart) {
        std::cerr << "ERROR: Failed to parse version from token \"" << rclLineView.substr(versionNumberStart, offset-versionNumberStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(versionNumberStart, offset-versionNumberStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse version from token \"" << rclLineView.substr(versionNumberStart, offset-versionNumberStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(versionNumberStart, offset-versionNumberStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope end \")\" at position " << offset << "." << std::endl;
    }
    offset++; // Skip ')'
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing end of command or goalie separator." << std::endl;
        return false;
    }
    //
    // Goalie [Optional, if not already present]
    //
    if (rclLine.compare(offset, 8, "(goalie)") == 0) {
        if (goalie) {
            std::cerr << "WARNING: Duplicated (goalie) token at InitCommand body." << std::endl;
        }
        else {
            goalie = true;
        }
        offset += 8;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing end of command." << std::endl;
        return false;
    }
    //
    // Command end
    //
    size_t endOfCommandStart = offset;
    offset = rclLine.find(')', endOfCommandStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid InitCommand body." << std::endl;
        std::cerr << "\tMissing end of command." << std::endl;
        return false;
    }
    if (offset != endOfCommandStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(endOfCommandStart, offset-endOfCommandStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope end \")\" at position " << offset << "." << std::endl;
    }
    offset++; // Skip ')'

    //
    // Semantic checks
    //
    constexpr const char* TEAMNAME_VALID_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+";
    constexpr const size_t TEAMNAME_MAX_LENGTH = 15;
    if (teamName.find_first_not_of(TEAMNAME_VALID_CHARS) != std::string::npos) {
        std::cerr << "ERROR: Invalid InitCommand \"" << rclLineView.substr(initStart, offset) << "\"." << std::endl;
        std::cerr << "\tInvalid Team Name (" << teamName << ")." << std::endl;
        return false;
    }
    if (teamName.length() > TEAMNAME_MAX_LENGTH) {
        std::cerr << "ERROR: Invalid InitCommand \"" << rclLineView.substr(initStart, offset) << "\"." << std::endl;
        std::cerr << "\tExceeds maximum Team Name length (15)." << std::endl;
        return false;
    }
    if (versionNumber < 0.0) {
        std::cerr << "ERROR: Invalid InitCommand \"" << rclLineView.substr(initStart, offset) << "\"." << std::endl;
        std::cerr << "\tVersion number (" << versionNumber << ") must be positive" << std::endl;
    }

    if (listener) listener->onPlayerInitCommand(std::move(teamName), goalie, versionNumber);

    return true;
}

bool
RCLParser::parseReconnect(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t reconnectStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid ReconnectCommand body." << std::endl;
        std::cerr << "\tMissing team name." << std::endl;
        return false;
    }

    //
    // Team Name
    //
    size_t teamNameStart = offset;
    while (offset < rclLine.length() && !std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid ReconnectCommand body." << std::endl;
        std::cerr << "\tMissing team name/uniform number separator." << std::endl;
        return false;
    }
    std::string teamName = rclLine.substr(teamNameStart, offset-teamNameStart);
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid ReconnectCommand body." << std::endl;
        std::cerr << "\tMissing team uniform number." << std::endl;
        return false;
    }
    //
    // Uniform Number
    //
    size_t uniformNumberStart = offset;
    int uniformNumber;
    offset = rclLine.find(')', uniformNumberStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid ReconnectCommand body." << std::endl;
        std::cerr << "\tMissing end of scope \")\"." << std::endl;
        return false;
    }
    auto result = std::from_chars(rclLine.data()+uniformNumberStart, rclLine.data()+offset, uniformNumber);
    if (result.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Failed to parse uniform number from token \"" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\" into int." << std::endl;
        return false;
    }
    if (result.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse uniform number from token \"" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << " is out of int range." << std::endl;
        return false;
    }
    if (result.ptr != rclLine.data() + offset) {
        size_t junkStart = result.ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    //
    // Semantic checks
    //
    constexpr const char* TEAMNAME_VALID_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+";
    constexpr const size_t TEAMNAME_MAX_LENGTH = 15;
    if (teamName.find_first_not_of(TEAMNAME_VALID_CHARS) != std::string::npos) {
        std::cerr << "ERROR: Invalid ReconnectCommand \"" << rclLineView.substr(reconnectStart, offset) << "\"." << std::endl;
        std::cerr << "\tInvalid Team Name (" << teamName << ")." << std::endl;
        return false;
    }
    if (teamName.length() > TEAMNAME_MAX_LENGTH) {
        std::cerr << "ERROR: Invalid ReconnectCommand \"" << rclLineView.substr(reconnectStart, offset) << "\"." << std::endl;
        std::cerr << "\tExceeds maximum Team Name length (15)." << std::endl;
        return false;
    }
    if (uniformNumber < 1 || uniformNumber > 11) {
        std::cerr << "ERROR: Invalid ReconnectCommand \"" << rclLineView.substr(reconnectStart, offset) << "\"." << std::endl;
        std::cerr << "\tUniform number (" << uniformNumber << ") must be in range [1,11]." << std::endl;
        return false;
    }

    if (listener) listener->onPlayerReconnectCommand(std::move(teamName), uniformNumber);

    return true;
}

bool 
RCLParser::parseDash(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid DashCommand body." << std::endl;
        std::cerr << "\tMissing parameters." << std::endl;
        return false;
    }

    //
    // Power
    //
    size_t powerStart = offset;
    offset = rclLine.find_first_of(" )", powerStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid DashCommand body." << std::endl;
        std::cerr << "\tMissing power/direction delimiter." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double power = std::strtod(rclLineView.data() + powerStart, &ptr);
    if (ptr == rclLineView.data() + powerStart) {
        std::cerr << "ERROR: Failed to parse power from token \"" << rclLineView.substr(powerStart, offset-powerStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(powerStart, offset-powerStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse power from token \"" << rclLineView.substr(powerStart, offset-powerStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(powerStart, offset-powerStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        if (rclLine[offset] == ' ') std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
        else std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid DashCommand body." << std::endl;
        std::cerr << "\tMissing command end \")\"." << std::endl;
        return false;
    }
    //
    // Direction [Optional]
    //
    size_t directionStart = offset;
    offset = rclLine.find(')', directionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid DashCommand body." << std::endl;
        std::cerr << "\tMissing command end \")\"." << std::endl;
        return false;
    }
    double direction = 0.0;
    if (offset > directionStart) {
        errno = 0; // Reset global error code
        direction = std::strtod(rclLineView.data() + directionStart, &ptr);
        if (ptr == rclLineView.data() + directionStart) {
            std::cerr << "WARNING: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
            std::cerr << "\tCould not convert \""<< rclLineView.substr(directionStart, offset-directionStart) << "\" into double." << std::endl;
        }
        if (errno == ERANGE) {
            std::cerr << "WARNING: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
            std::cerr << "\t" << rclLineView.substr(directionStart, offset-directionStart) << " is out of double range." << std::endl;
        }
        if (ptr != rclLine.data()+offset) {
            size_t junkStart = ptr-rclLine.data();
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
            std::cerr << "\tSkip to close scope \")\" at position " << offset << "." << std::endl;
        }
    }

    offset++; // Skip ')'

    if (listener) listener->onDashCommand(power, direction);

    return true;
}

bool 
RCLParser::parseTurn(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t turnStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid TurnCommand body." << std::endl;
        std::cerr << "\tMissing moment parameter." << std::endl;
        return false;
    }

    //
    // Moment
    //
    size_t momentStart = offset;
    offset = rclLine.find_first_of(')', momentStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid TurnCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double moment = std::strtod(rclLineView.data() + momentStart, &ptr);
    if (ptr == rclLineView.data() + momentStart) {
        std::cerr << "ERROR: Failed to parse moment from token \"" << rclLineView.substr(momentStart, offset-momentStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(momentStart, offset-momentStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse moment from token \"" << rclLineView.substr(momentStart, offset-momentStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(momentStart, offset-momentStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onTurnCommand(moment);

    return true;
}

bool 
RCLParser::parseKick(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t kickStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid KickCommand body." << std::endl;
        std::cerr << "\tMissing parameters." << std::endl;
        return false;
    }

    //
    // Power
    //
    size_t powerStart = offset;
    offset = rclLine.find(' ', powerStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid KickCommand body." << std::endl;
        std::cerr << "\tMissing power/direction delimiter." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double power = std::strtod(rclLineView.data() + powerStart, &ptr);
    if (ptr == rclLineView.data() + powerStart) {
        std::cerr << "ERROR: Failed to parse power from token \"" << rclLineView.substr(powerStart, offset-powerStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(powerStart, offset-powerStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse power from token \"" << rclLineView.substr(powerStart, offset-powerStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(powerStart, offset-powerStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid KickCommand body." << std::endl;
        std::cerr << "\tMissing direction." << std::endl;
        return false;
    }
    //
    // Direction
    //
    size_t directionStart = offset;
    offset = rclLine.find(')', directionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid KickCommand body." << std::endl;
        std::cerr << "\tMissing command end \")\"." << std::endl;
        return false;
    }
    errno = 0; // Reset global error code
    double direction = std::strtod(rclLineView.data() + directionStart, &ptr);
    if (ptr == rclLineView.data() + directionStart) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(directionStart, offset-directionStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(directionStart, offset-directionStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to close scope \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onKickCommand(power, direction);

    return true;
}

bool 
RCLParser::parseTackle(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
        std::cerr << "\tMissing direction parameter." << std::endl;
        return false;
    }

    //
    // Direction
    //
    size_t directionStart = offset;
    offset = rclLine.find_first_of(" )", directionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double direction = std::strtod(rclLineView.data() + directionStart, &ptr);
    if (ptr == rclLineView.data() + directionStart) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(directionStart, offset-directionStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(directionStart, offset-directionStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        if (rclLine[offset] == ' ') std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
        else std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    //
    // Foul [Optional]
    //
    bool foul = false;
    if (rclLine[offset] != ')') {
        size_t foulStart = offset;
        offset = rclLine.find_first_of(" )", foulStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
        }
        if (rclLine.compare(foulStart, offset-foulStart, "on") == 0 || rclLine.compare(foulStart, offset-foulStart, "true") == 0) {
            foul = true;
        } else if (rclLine.compare(foulStart, offset-foulStart, "off") == 0 || rclLine.compare(foulStart, offset-foulStart, "false") == 0) {
            foul = false;
        } else {
            std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
            std::cerr << "\tUnknown foul switch \"" << rclLine.substr() << "\"." << std::endl;
            return false;
        }
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
    }
    //
    // Command end
    //
    size_t commandEndStart = offset;
    offset = rclLine.find(')', commandEndStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid TackleCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (offset != commandEndStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onTackleCommand(direction, foul);

    return true;
}

bool 
RCLParser::parseCatch(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid CatchCommand body." << std::endl;
        std::cerr << "\tMissing direction parameter." << std::endl;
        return false;
    }

    //
    // Direction
    //
    size_t directionStart = offset;
    offset = rclLine.find(')', directionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid CatchCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double direction = std::strtod(rclLineView.data() + directionStart, &ptr);
    if (ptr == rclLineView.data() + directionStart) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(directionStart, offset-directionStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(directionStart, offset-directionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(directionStart, offset-directionStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onCatchCommand(direction);

    return true;
}

bool 
RCLParser::parseTurnNeck(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid TurnNeckCommand body." << std::endl;
        std::cerr << "\tMissing moment parameter." << std::endl;
        return false;
    }

    //
    // Moment
    //
    size_t momentStart = offset;
    offset = rclLine.find_first_of(')', momentStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid TurnNeckCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double moment = std::strtod(rclLineView.data() + momentStart, &ptr);
    if (ptr == rclLineView.data() + momentStart) {
        std::cerr << "ERROR: Failed to parse moment from token \"" << rclLineView.substr(momentStart, offset-momentStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(momentStart, offset-momentStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse moment from token \"" << rclLineView.substr(momentStart, offset-momentStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(momentStart, offset-momentStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onTurnNeckCommand(moment);

    return true;
}

bool
RCLParser::parseChangeView(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
        std::cerr << "\tMissing width." << std::endl;
        return false;
    }

    //
    // View Width
    //
    size_t widthStart = offset;
    offset = rclLine.find_first_of(" )", widthStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    ViewWidth width;
    if (rclLine.compare(widthStart, offset-widthStart, "narrow") == 0) {
        width = ViewWidth::Narrow;
    } else if (rclLine.compare(widthStart, offset-widthStart, "normal") == 0) {
        width = ViewWidth::Normal;
    } else if (rclLine.compare(widthStart, offset-widthStart, "wide") == 0) {
        width = ViewWidth::Wide;
    } else {
        std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
        std::cerr << "\tUnknown width \"" << rclLineView.substr(widthStart, offset-widthStart) << "\"." << std::endl;
        return false;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    //
    // View Quality [Optional]
    //
    ViewQuality quality = ViewQuality::High;
    if (rclLine[offset] != ')') {
        size_t qualityStart = offset;
        offset = rclLine.find_first_of(" )", qualityStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        if (rclLine.compare(qualityStart, offset-qualityStart, "low") == 0) {
            quality = ViewQuality::Low;
        } else if (rclLine.compare(qualityStart, offset-qualityStart, "high") == 0) {
            quality = ViewQuality::High;
        } else {
            std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
            std::cerr << "\tUnknown quality \"" << rclLineView.substr(qualityStart, offset-qualityStart) << "\"." << std::endl;
            return false;
        }
    }
    //
    // Command end
    //
    size_t commandEndStart = offset;
    offset = rclLine.find(')', commandEndStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid ChangeViewCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (offset != commandEndStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onChangeViewCommand(width, quality);

    return true;
}

bool
RCLParser::parseAttentionTo(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t attentionToStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
        std::cerr << "\tMissing command action." << std::endl;
        return false;
    }

    //
    // Command Action
    //
    AttentionToCommand command;
    size_t commandActionStart = offset;
    offset = rclLine.find_first_of(" )", commandActionStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (rclLine.compare(commandActionStart, offset-commandActionStart, "off") == 0) {
        //
        // "Off" action
        //
        command.turnOff();
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        //
        // Command end
        //
        size_t commandEndStart = offset;
        offset = rclLine.find(')', commandEndStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        if (offset != commandEndStart) {
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
            std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
        }
    } else {
        //
        // "On" Action
        //
        auto* target = new AttentionToTarget();
        //
        // Attention target side
        //
        size_t attentionSideStart = commandActionStart;
        if (rclLine.compare(attentionSideStart, offset-attentionSideStart, "our") == 0) {
            //
            // Our side
            //
            target->side = AttentionToTargetSide::Our;
        } else if (rclLine.compare(attentionSideStart, offset-attentionSideStart, "opp") == 0) {
            //
            // Opp side
            //
            target->side = AttentionToTargetSide::Opp;
        } else if (rclLine.compare(attentionSideStart, offset-attentionSideStart, "l") == 0) {
            //
            // Left side
            //
            target->side = AttentionToTargetSide::Left;
        } else if (rclLine.compare(attentionSideStart, offset-attentionSideStart, "r") == 0) {
            //
            // Right side
            //
            target->side = AttentionToTargetSide::Right;
        } else {
            // Unknown
            std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
            std::cerr << "\tUnknown attention side \"" << rclLineView.substr(attentionSideStart, offset-attentionSideStart) << "\"." << std::endl;
            return false;
        }
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
            std::cerr << "\tMissing attention target uniform number." << std::endl;
            return false;
        }
        //
        // Attention target uniform number
        //
        size_t uniformNumberStart = offset;
        offset = rclLine.find(')', uniformNumberStart);
        if (offset == std::string_view::npos) {
            std::cerr << "ERROR: Invalid AttentionToCommand body." << std::endl;
            std::cerr << "\tMissing end of command \")\"." << std::endl;
            return false;
        }
        auto result = std::from_chars(rclLine.data()+uniformNumberStart, rclLine.data()+offset, target->uniformNumber);
        if (result.ec == std::errc::invalid_argument) {
            std::cerr << "ERROR: Failed to parse uniform number from token \"" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\"." << std::endl;
            std::cerr << "\tCould not convert \""<< rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\" into int." << std::endl;
            return false;
        }
        if (result.ec == std::errc::result_out_of_range) {
            std::cerr << "ERROR: Failed to parse uniform number from token \"" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << "\"." << std::endl;
            std::cerr << "\t" << rclLineView.substr(uniformNumberStart, offset-uniformNumberStart) << " is out of int range." << std::endl;
            return false;
        }
        if (result.ptr != rclLine.data() + offset) {
            size_t junkStart = result.ptr-rclLine.data();
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
            std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
        }

        //
        // Semantic checks
        //
        if (target->uniformNumber < 1 || target->uniformNumber > 11) {
            std::cerr << "ERROR: Invalid AttentionToCommand \"" << rclLineView.substr(attentionToStart, offset) << "\"." << std::endl;
            std::cerr << "\tUniform number (" << target->uniformNumber << ") must be in range [1,11]." << std::endl;
            return false;
        }

        command.turnOn(std::unique_ptr<AttentionToTarget>(target));
    }

    offset++; // Skip ')'

    if (listener) listener->onAttentionToCommand(std::move(command));

    return true;
}

bool
RCLParser::parsePointTo(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t pointToStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
        std::cerr << "\tMissing command action." << std::endl;
        return false;
    }

    //
    // Command Action
    //
    PointToCommand command;
    size_t commandActionStart = offset;
    offset = rclLine.find_first_of(" )", commandActionStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (rclLine.compare(commandActionStart, offset-commandActionStart, "off") == 0) {
        //
        // "Off" action
        //
        command.turnOff();
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        //
        // Command end
        //
        size_t commandEndStart = offset;
        offset = rclLine.find(')', commandEndStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        if (offset != commandEndStart) {
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
            std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
        }
    } else {
        //
        // "On" Action
        //
        auto* target = new PointToTarget();
        //
        // Point distance
        //
        size_t pointDistanceStart = commandActionStart;
        offset = rclLine.find(' ', pointDistanceStart);
        if (offset == std::string_view::npos) {
            std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
            std::cerr << "\tMissing Distance/Direction delimiter." << std::endl;
            return false;
        }
        char* ptr = nullptr;
        errno = 0; // Reset global error code
        double distance = std::strtod(rclLineView.data() + pointDistanceStart, &ptr);
        if (ptr == rclLineView.data() + pointDistanceStart) {
            std::cerr << "ERROR: Failed to parse distance from token \"" << rclLineView.substr(pointDistanceStart, offset-pointDistanceStart) << "\"." << std::endl;
            std::cerr << "\tCould not convert \""<< rclLineView.substr(pointDistanceStart, offset-pointDistanceStart) << "\" into double." << std::endl;
            return false;
        }
        if (errno == ERANGE) {
            std::cerr << "ERROR: Failed to parse distance from token \"" << rclLineView.substr(pointDistanceStart, offset-pointDistanceStart) << "\"." << std::endl;
            std::cerr << "\t" << rclLineView.substr(pointDistanceStart, offset-pointDistanceStart) << " is out of double range." << std::endl;
            return false;
        }
        if (ptr != rclLine.data()+offset) {
            size_t junkStart = ptr-rclLine.data();
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
            std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
        }
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid MoveCommand body." << std::endl;
            std::cerr << "\tMissing y-coordinate." << std::endl;
            return false;
        }
        //
        // Direction
        //
        size_t pointDirectionStart = offset;
        offset = rclLine.find(')', pointDirectionStart);
        if (offset == std::string_view::npos) {
            std::cerr << "ERROR: Invalid PointToCommand body." << std::endl;
            std::cerr << "\tMissing command end \")\"." << std::endl;
            return false;
        }
        errno = 0; // Reset global error code
        double direction = std::strtod(rclLineView.data() + pointDirectionStart, &ptr);
        if (ptr == rclLineView.data() + pointDirectionStart) {
            std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(pointDirectionStart, offset-pointDirectionStart) << "\"." << std::endl;
            std::cerr << "\tCould not convert \""<< rclLineView.substr(pointDirectionStart, offset-pointDirectionStart) << "\" into double." << std::endl;
            return false;
        }
        if (errno == ERANGE) {
            std::cerr << "ERROR: Failed to parse direction from token \"" << rclLineView.substr(pointDirectionStart, offset-pointDirectionStart) << "\"." << std::endl;
            std::cerr << "\t" << rclLineView.substr(pointDirectionStart, offset-pointDirectionStart) << " is out of double range." << std::endl;
            return false;
        }
        if (ptr != rclLine.data()+offset) {
            size_t junkStart = ptr-rclLine.data();
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
            std::cerr << "\tSkip to close scope \")\" at position " << offset << "." << std::endl;
        }

        command.turnOn(std::unique_ptr<PointToTarget>(new PointToTarget{distance, direction}));
    }

    offset++; // Skip ')'

    if (listener) listener->onPointToCommand(std::move(command));

    return true;
}

bool
RCLParser::parseSay(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    if (offset >= rclLine.length() || rclLine[offset] != '"') {
        std::cerr << "ERROR: Invalid SayCommand body." << std::endl;
        std::cerr << "\tMissing say message." << std::endl;
        return false;
    }
    offset++; // Skip '"'
    //
    // Message
    //
    size_t messageStart = offset;
    // Advance until find end of string literal
    offset = rclLine.find('"', messageStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid SayCommand body." << std::endl;
        std::cerr << "\tMissing say message end '\"'." << std::endl;
        return false;
    }
    auto sayMessage = rclLine.substr(messageStart, offset-messageStart);
    offset++; // Skip '"'
    //
    // Command end
    //
    size_t commandEndStart = offset;
    offset = rclLine.find(')', commandEndStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid SayCommand body." << std::endl;
        std::cerr << "\tMissing command end ')'." << std::endl;
        return false;
    }
    if (offset != commandEndStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
        std::cerr << "\tSkip to \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onSayCommand(std::move(sayMessage));

    return true;
}

bool
RCLParser::parseEar(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t earStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command action." << std::endl;
        return false;
    }

    //
    // Scope Start
    //
    size_t paramsScopeStart = offset;
    offset = rclLine.find('(', paramsScopeStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command parameters." << std::endl;
        return false;
    }
    if (offset != paramsScopeStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(paramsScopeStart, offset-paramsScopeStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope start '(' at position " << offset << "." << std::endl;
    }
    offset++; // Skip '('

    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }

    //
    // Command Action
    //
    EarCommand command;
    size_t commandActionStart = offset;
    offset = rclLine.find_first_of(" )", commandActionStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (rclLine.compare(commandActionStart, offset-commandActionStart, "off") == 0) {
        //
        // "Off" action
        //
        command.setAction(EarCommandAction::Off);
    } else if (rclLine.compare(commandActionStart, offset-commandActionStart, "on") == 0){
        //
        // "On" Action
        //
        command.setAction(EarCommandAction::On);
    } else {
        //
        // Unknown Action
        //
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tUnknown action \"" << rclLineView.substr(commandActionStart, offset-commandActionStart) << "\"." << std::endl;
        return false;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    //
    // Command target and type [Optional]
    //
    if (rclLine[offset] != ')') {
        std::unique_ptr<EarCommandParams> commandParameters(new EarCommandParams());
        //
        // Command target
        //
        size_t commandTargetStart = offset;
        offset = rclLine.find_first_of(" )", commandTargetStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        if (rclLine.compare(commandTargetStart, offset-commandTargetStart, "our") == 0)
        {
            //
            // Target Our
            //
            commandParameters->setTarget(EarCommandEnumTarget::Our);
        }
        else if (rclLine.compare(commandTargetStart, offset-commandTargetStart, "opp") == 0)
        {
            //
            // Target Opp
            //
            commandParameters->setTarget(EarCommandEnumTarget::Opp);
        }
        else if (rclLine.compare(commandTargetStart, offset-commandTargetStart, "l") == 0
        || rclLine.compare(commandTargetStart, offset-commandTargetStart, "left") == 0)
        {
            //
            // Target Left
            //
            commandParameters->setTarget(EarCommandEnumTarget::Left);
        }
        else if (rclLine.compare(commandTargetStart, offset-commandTargetStart, "r") == 0 
        || rclLine.compare(commandTargetStart, offset-commandTargetStart, "right") == 0)
        {
            //
            // Target Right
            //
            commandParameters->setTarget(EarCommandEnumTarget::Right);
        }
        else
        {
            //
            // Target TeamName
            //
            commandParameters->setTarget(rclLine.substr(commandTargetStart, offset-commandTargetStart));
        }
        //
        // Whitespace
        //
        while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
        if (offset == rclLine.length()) {
            std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
            std::cerr << "\tMissing command end." << std::endl;
            return false;
        }
        //
        // Command type [Optional]
        //
        if (rclLine[offset] != ')') {
            size_t commandTypeStart = offset;
            offset = rclLine.find_first_of(" )", commandTypeStart);
            if (offset == std::string::npos) {
                std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
                std::cerr << "\tMissing command end." << std::endl;
                return false;
            }
            if (rclLine.compare(commandTypeStart, offset-commandTypeStart, "complete") == 0
            || rclLine.compare(commandTypeStart, offset-commandTypeStart, "c") == 0)
            {
                //
                // Type Complete
                //
                commandParameters->setType(EarCommandType::Complete);
            } 
            else if (rclLine.compare(commandTypeStart, offset-commandTypeStart, "partial") == 0
            || rclLine.compare(commandTypeStart, offset-commandTypeStart, "p") == 0)
            {
                //
                // Type Partial
                //
                commandParameters->setType(EarCommandType::Partial);
            } 
            else
            {
                //
                // Unknown Type
                //
                std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
                std::cerr << "\tUnknown ear type \"" << rclLineView.substr(commandTypeStart, offset-commandTypeStart) << "\"." << std::endl;
                return false;
            }
        } else {
            // Set defaults for type
            commandParameters->setType(EarCommandType::CompleteAndPartial);
        }
        //
        // Scope end
        //
        size_t paramsScopeEndStart = offset;
        offset = rclLine.find(')', paramsScopeEndStart);
        if (offset == std::string::npos) {
            std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
            std::cerr << "\tMissing params scope end." << std::endl;
            return false;
        }
        if (offset != paramsScopeEndStart) {
            std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(paramsScopeEndStart, offset-paramsScopeEndStart) << "\"." << std::endl;
            std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
        }
        
        offset++; // skip ')'

        //
        // Semantic Checks
        //
        if (commandParameters->hasTeamNameTarget()) {
            const std::string teamName = commandParameters->getTeamName().value();
            constexpr const char* TEAMNAME_VALID_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-+";
            constexpr const size_t TEAMNAME_MAX_LENGTH = 15;
            if (teamName.find_first_not_of(TEAMNAME_VALID_CHARS) != std::string::npos) {
                std::cerr << "ERROR: Invalid EarCommand body \"" << rclLineView.substr(earStart, offset-earStart) << "\"." << std::endl;
                std::cerr << "\tInvalid Team Name." << std::endl;
                return false;
            }
            if (teamName.length() > TEAMNAME_MAX_LENGTH) {
                std::cerr << "ERROR: Invalid EarCommand body \"" << rclLineView.substr(earStart, offset-earStart) << "\"." << std::endl;
                std::cerr << "\tTeam Name \"" << teamName << "\" exceeds maximum length (15)." << std::endl;
                return false;
            }
        }
        
        command.setParams(std::move(commandParameters));
    } else {
        offset++; // Skip ')'
        // Set defaults for target and type
        command.setParams(std::unique_ptr<EarCommandParams>(new EarCommandParams()));
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    //
    // Command end
    //
    size_t commandEndStart = offset;
    offset = rclLine.find(')', commandEndStart);
    if (offset == std::string::npos) {
        std::cerr << "ERROR: Invalid EarCommand body." << std::endl;
        std::cerr << "\tMissing command end." << std::endl;
        return false;
    }
    if (offset != commandEndStart) {
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(commandEndStart, offset-commandEndStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onEarCommand(std::move(command));

    return true;
}

bool
RCLParser::parseMove(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t moveStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid MoveCommand body." << std::endl;
        std::cerr << "\tMissing coordinates." << std::endl;
        return false;
    }

    //
    // X-Coordinate
    //
    size_t xStart = offset;
    offset = rclLine.find(' ', xStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid MoveCommand body." << std::endl;
        std::cerr << "\tMissing Coordinate x/y delimiter." << std::endl;
        return false;
    }
    char* ptr = nullptr;
    errno = 0; // Reset global error code
    double x = std::strtod(rclLineView.data() + xStart, &ptr);
    if (ptr == rclLineView.data() + xStart) {
        std::cerr << "ERROR: Failed to parse x-coordinate from token \"" << rclLineView.substr(xStart, offset-xStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(xStart, offset-xStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse x-coordinate from token \"" << rclLineView.substr(xStart, offset-xStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(xStart, offset-xStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid MoveCommand body." << std::endl;
        std::cerr << "\tMissing y-coordinate." << std::endl;
        return false;
    }
    //
    // Y-Coordinate
    //
    size_t yStart = offset;
    offset = rclLine.find(')', yStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid MoveCommand body." << std::endl;
        std::cerr << "\tMissing command end \")\"." << std::endl;
        return false;
    }
    errno = 0; // Reset global error code
    double y = std::strtod(rclLineView.data() + yStart, &ptr);
    if (ptr == rclLineView.data() + yStart) {
        std::cerr << "ERROR: Failed to parse y-coordinate from token \"" << rclLineView.substr(yStart, offset-yStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(yStart, offset-yStart) << "\" into double." << std::endl;
        return false;
    }
    if (errno == ERANGE) {
        std::cerr << "ERROR: Failed to parse y-coordinate from token \"" << rclLineView.substr(yStart, offset-yStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(yStart, offset-yStart) << " is out of double range." << std::endl;
        return false;
    }
    if (ptr != rclLine.data()+offset) {
        size_t junkStart = ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to close scope \")\" at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    if (listener) listener->onMoveCommand(x,y);

    return true;
}

bool
RCLParser::parseClang(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t clangStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing version description." << std::endl;
        return false;
    }
    size_t versionStart = rclLine.find("(ver ", offset);
    if (versionStart == std::string_view::npos) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing version start \"(ver \"." << std::endl;
        return false;
    }
    if (versionStart != clangStart) {
        std::cerr << "WARNING: Found junk before Clang version start \"" << rclLineView.substr(clangStart,versionStart-clangStart) << "\"." << std::endl;
        std::cerr << "\tSkipping \"" << rclLineView.substr(clangStart,versionStart-clangStart) << "\"." <<std::endl;
        offset = versionStart;
    }
    offset += 4; // Skip "(ver"
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing Clang minimum version." << std::endl;
        return false;
    }
    //
    // Clang Minimum Version
    //
    int clangMinVersion;
    size_t minVersionStart = offset;
    offset = rclLine.find_first_of(' ', minVersionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing Clang minimum/maximum version delimiter." << std::endl;
        return false;
    }
    auto result = std::from_chars(rclLine.data()+minVersionStart, rclLine.data()+offset, clangMinVersion);
    if (result.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Failed to parse clang minimum version from token \"" << rclLineView.substr(minVersionStart, offset-minVersionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(minVersionStart, offset-minVersionStart) << "\" into int." << std::endl;
        return false;
    }
    if (result.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse clang maximum version from token \"" << rclLineView.substr(minVersionStart, offset-minVersionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(minVersionStart, offset-minVersionStart) << " is out of int range." << std::endl;
        return false;
    }
    if (result.ptr != rclLine.data()+offset) {
        size_t junkStart = result.ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to whitespace at position " << offset << "." << std::endl;
    }
    //
    // Whitespace
    //
    while (offset < rclLine.length() && std::isspace(rclLine[offset])) offset++;
    if (offset == rclLine.length()) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing Clang maximum version." << std::endl;
        return false;
    }
    //
    // Clang Maximum Version
    //
    int clangMaxVersion;
    size_t maxVersionStart = offset;
    offset = rclLine.find("))", maxVersionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid ClangCommand body." << std::endl;
        std::cerr << "\tMissing command end \"))\"." << std::endl;
        return false;
    }
    result = std::from_chars(rclLine.data()+maxVersionStart, rclLine.data()+offset, clangMaxVersion);
    if (result.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Failed to parse clang maximum version from token \"" << rclLineView.substr(maxVersionStart, offset-maxVersionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(maxVersionStart, offset-maxVersionStart) << "\" into int." << std::endl;
        return false;
    }
    if (result.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse clang maximum version from token \"" << rclLineView.substr(maxVersionStart, offset-maxVersionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(maxVersionStart, offset-maxVersionStart) << " is out of int range." << std::endl;
        return false;
    }
    if (result.ptr != rclLine.data()+offset) {
        size_t junkStart = result.ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to close scope \")\" at position " << offset << "." << std::endl;
    }

    offset += 2; // Skip "))"

    //
    // Semantic checks
    //
    if (clangMinVersion < 0) {
        std::cerr << "ERROR: Invalid Clang minimum version (" << clangMinVersion << ")." << std::endl;
        std::cerr << "\tMust be a positive integer." << std::endl;
        return false;
    }
    if (clangMaxVersion < clangMinVersion) {
        std::cerr << "ERROR: Invalid Clang maximum version (" << clangMaxVersion << ")." << std::endl;
        std::cerr << "\tMust be >= Clang minimum version (" << clangMinVersion << ")." << std::endl;
        return false;
    }

    if (listener) listener->onClangCommand(clangMinVersion, clangMaxVersion);

    return true;
}

bool
RCLParser::parseCompression(const std::string& rclLine, size_t& offset) noexcept {
    const std::string_view rclLineView = rclLine;
    size_t compressionStart = offset;
    if (offset >= rclLine.length()) {
        std::cerr << "ERROR: Invalid CompressionCommand body." << std::endl;
        std::cerr << "\tMissing compression level." << std::endl;
        return false;
    }
    int compressionLevel;
    offset = rclLine.find(')', compressionStart);
    if (offset == std::string_view::npos) {
        std::cerr << "ERROR: Invalid CompressionCommand body." << std::endl;
        std::cerr << "\tMissing end of scope \")\"." << std::endl;
        return false;
    }
    auto levelResult = std::from_chars(rclLine.data()+compressionStart, rclLine.data()+offset, compressionLevel);
    if (levelResult.ec == std::errc::invalid_argument) {
        std::cerr << "ERROR: Failed to parse compression level from token \"" << rclLineView.substr(compressionStart, offset-compressionStart) << "\"." << std::endl;
        std::cerr << "\tCould not convert \""<< rclLineView.substr(compressionStart, offset-compressionStart) << "\" into int." << std::endl;
        return false;
    }
    if (levelResult.ec == std::errc::result_out_of_range) {
        std::cerr << "ERROR: Failed to parse compression level from token \"" << rclLineView.substr(compressionStart, offset-compressionStart) << "\"." << std::endl;
        std::cerr << "\t" << rclLineView.substr(compressionStart, offset-compressionStart) << " is out of int range." << std::endl;
        return false;
    }
    if (levelResult.ptr != rclLine.data() + offset) {
        size_t junkStart = levelResult.ptr-rclLine.data();
        std::cerr << "WARNING: Found trailing junk \"" << rclLineView.substr(junkStart, offset-junkStart) << "\"." << std::endl;
        std::cerr << "\tSkip to scope end ')' at position " << offset << "." << std::endl;
    }

    offset++; // Skip ')'

    //
    // Semantic checks
    //
    if (compressionLevel < 1 || compressionLevel > 9) {
        std::cerr << "ERROR: Invalid Compression Level (" << compressionLevel << ")." << std::endl;
        std::cerr << "\tMust be in range [1,9]" << std::endl;
        return false;
    }

    if (listener) listener->onCompressionCommand(compressionLevel);

    return true;
}
