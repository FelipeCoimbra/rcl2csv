#pragma once

#include <iostream>
#include <memory>
#include <optional>
#include <string>

#include "rcss2d.hpp"

class RCLParsingListener {
public:
    RCLParsingListener() = default;
    virtual ~RCLParsingListener() {}
    /**
     * @brief Notification call executed for every new succesfully read line.
     * @param lineNumber  
     */
    virtual void onRCLEntry(size_t lineNumber, const std::string& completeLine) noexcept {}
    /**
     * @brief Notification call executed for every new succesfully parsed command time.
     */
    virtual void onCommandTime(CommandTime&& time) noexcept {}
    /**
     * @brief Notification call executed for every new succesfully parsed player client identification.
     */
    virtual void onPlayerClient(std::string&& teamName, int uniformNumber) noexcept {}
    /**
     * @brief Notification call executed for every new succesfully parsed online coach client identification.
     */
    virtual void onOnlineCoachClient(std::string&& teamName) noexcept {}
    /**
     * @brief Notification call executed for every new succesfully parsed offline coach client identification.
     */
    virtual void onOfflineCoachClient(std::string&&teamName) noexcept {}
    // TODO: Add support for more events
    //
    // Player Commands
    //
    virtual void onPlayerInitCommand(std::string teamName, bool isGoalie, double version) noexcept {}
    virtual void onPlayerReconnectCommand(std::string teamName, int uniformNumber) noexcept {}
    virtual void onDashCommand(double power, double direction) noexcept {}
    virtual void onTurnCommand(double moment) noexcept {}
    virtual void onKickCommand(double power, double direction) noexcept {}
    virtual void onTackleCommand(double direction, bool foul) noexcept {}
    virtual void onCatchCommand(double direction) noexcept {}
    virtual void onTurnNeckCommand(double moment) noexcept {}
    virtual void onChangeViewCommand(ViewWidth viewWidth, ViewQuality viewQuality) noexcept {}
    virtual void onAttentionToCommand(AttentionToCommand attentionToCommand) noexcept {}
    virtual void onPointToCommand(PointToCommand pointToCommand) noexcept {}
    virtual void onSayCommand(std::string sayMessage) noexcept {}
    virtual void onEarCommand(EarCommand earCommand) noexcept {}
    virtual void onMoveCommand(double x, double y) noexcept {}
    virtual void onScoreCommand() noexcept {}
    virtual void onClangCommand(int clangMinVersion, int clangMaxVersion) noexcept {}
    virtual void onSynchSeeCommand() noexcept {}
    //
    // Player and OnlineCoach Commands
    //
    virtual void onByeCommand() noexcept {}
    //
    // Common Commands
    //  These are shared by all ClientTypes
    //
    virtual void onCompressionCommand(int compressionLevel) noexcept {}
    virtual void onDoneCommand() noexcept {}
};

class RCLParser {
public:
    RCLParser() = default;
    void parse(std::istream& logStreamer) noexcept;
    void setListener(std::unique_ptr<RCLParsingListener>&& newListener) noexcept {
        listener = std::forward<std::unique_ptr<RCLParsingListener>>(newListener);
    }

private:
    std::optional<std::string_view> extractTime(const std::string& rclLine, size_t& offset) noexcept;
    bool parseTime(const std::string_view& timeDescription) noexcept;
    std::optional<std::string_view> extractClient(const std::string& rclLine, size_t& offset) noexcept;
    bool parseClient(const std::string_view& clientDescription) noexcept;
    bool parsePlayerCommands(const std::string& rclLine, size_t& offset) noexcept;
    //
    // Player Commands
    //
    bool parsePlayerInit(const std::string& rclLine, size_t& offset) noexcept;
    bool parseReconnect(const std::string& rclLine, size_t& offset) noexcept;
    bool parseDash(const std::string& rclLine, size_t& offset) noexcept;
    bool parseTurn(const std::string& rclLine, size_t& offset) noexcept;
    bool parseKick(const std::string& rclLine, size_t& offset) noexcept;
    bool parseTackle(const std::string& rclLine, size_t& offset) noexcept;
    bool parseCatch(const std::string& rclLine, size_t& offset) noexcept;
    bool parseTurnNeck(const std::string& rclLine, size_t& offset) noexcept;
    bool parseChangeView(const std::string& rclLine, size_t& offset) noexcept;
    bool parseAttentionTo(const std::string& rclLine, size_t& offset) noexcept;
    bool parsePointTo(const std::string& rclLine, size_t& offset) noexcept;
    bool parseSay(const std::string& rclLine, size_t& offset) noexcept;
    bool parseEar(const std::string& rclLine, size_t& offset) noexcept;
    bool parseMove(const std::string& rclLine, size_t& offset) noexcept;
    bool parseClang(const std::string& rclLine, size_t& offset) noexcept;
    //
    // Common Commands
    //
    bool parseCompression(const std::string& rclLine, size_t& offset) noexcept;

    size_t lineCount = 0;
    CommandTimeTracker timeTracker;
    ClientType lastTypeParsed;
    std::unique_ptr<RCLParsingListener> listener;
};

