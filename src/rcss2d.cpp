
#include "rcss2d.hpp"

std::ostream& operator<<(std::ostream& os, ClientType type) {
    switch (type) 
    {
    case ClientType::Player:
        os << "Player";
        break;
    case ClientType::OnlineCoach:
        os << "OnlineCoach";
        break;
    case ClientType::OfflineCoach:
        os << "OfflineCoach";
        break;
    default:
        os << "[Unknown ClientType]";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, ViewWidth width) {
    switch (width) 
    {
    case ViewWidth::Narrow:
        os << "narrow";
        break;
    case ViewWidth::Normal:
        os << "normal";
        break;
    case ViewWidth::Wide:
        os << "wide";
        break;
    default:
        os << "[Unknown ViewWidth]";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, ViewQuality quality) {
    switch (quality) 
    {
    case ViewQuality::Low:
        os << "low";
        break;
    case ViewQuality::High:
        os << "high";
        break;
    default:
        os << "[Unknown ViewQuality]";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, AttentionToTargetSide side) {
    switch (side) 
    {
    case AttentionToTargetSide::Left:
        os << "l";
        break;
    case AttentionToTargetSide::Right:
        os << "r";
        break;
    case AttentionToTargetSide::Our:
        os << "our";
            break;
    case AttentionToTargetSide::Opp:
        os << "opp";
        break;
    default:
        os << "[Unknown AttentionToTargetSide]";
    }
    return os;
}

std::optional<AttentionToTarget> 
AttentionToCommand::getTarget() const noexcept {
    if (!target) return {}; 
    return {*target};
}

std::optional<PointToTarget> 
PointToCommand::getTarget() const noexcept {
    if (!target) return {}; 
    return {*target};
}

std::ostream& operator<<(std::ostream& os, EarCommandAction action) {
    switch (action) 
    {
    case EarCommandAction::On:
        os << "On";
        break;
    case EarCommandAction::Off:
        os << "Off";
        break;
    default:
        os << "[Unknown EarCommandAction]";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, EarCommandEnumTarget action) {
    switch (action) 
    {
    case EarCommandEnumTarget::Both:
        os << "Both";
        break;
    case EarCommandEnumTarget::Our:
        os << "Our";
        break;
    case EarCommandEnumTarget::Opp:
        os << "Opp";
        break;
    case EarCommandEnumTarget::Left:
        os << "Left";
        break;
    case EarCommandEnumTarget::Right:
        os << "Right";
        break;
    default:
        os << "[Unknown EarCommandEnumTarget]";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, EarCommandType action) {
    switch (action) 
    {
    case EarCommandType::CompleteAndPartial:
        os << "CompleteAndPartial";
        break;
    case EarCommandType::Complete:
        os << "Complete";
        break;
    case EarCommandType::Partial:
        os << "Partial";
        break;
    default:
        os << "[Unknown EarCommandType]";
    }
    return os;
}

std::optional<EarCommandEnumTarget> 
EarCommandParams::getEnumTarget() const noexcept {
    if (hasEnumTarget()) {
        return get<EarCommandEnumTarget>(target);
    }
    return {};
}

std::optional<std::string> 
EarCommandParams::getTeamName() const noexcept {
    if (hasTeamNameTarget()) {
        return get<std::string>(target);
    }
    return {};
}

std::optional<EarCommandParams>
EarCommand::getParams() const noexcept {
    if (!parameters) return {}; 
    return {*parameters};
}
