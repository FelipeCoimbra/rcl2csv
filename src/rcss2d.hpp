#pragma once

#include <iostream>
#include <optional>
#include <variant>

struct CommandTime {
    int runningStep;    //<! Number of running game cycles so far
    int stoppedStep;    //<! Number of stopped game cycles for the current running step.
    size_t globalCommandCount;   //<! Global order of commands processed by the rcssserver
};

/**
 * @class CommandTimeTracker
 * @brief Calculates CommandTimes using an internal tracker to generate sequential times.
 */
class CommandTimeTracker {
public:
    CommandTimeTracker()
    : callCount(0)
    {}

    CommandTime nextTime(int running, int stopped) noexcept {
        return CommandTime{running,stopped,++callCount};
    }
private:
    size_t callCount;
};

enum class ClientType {
    Player,
    OnlineCoach,
    OfflineCoach
};

std::ostream& operator<<(std::ostream& os, ClientType type);

enum class ViewWidth {
    Narrow,
    Normal,
    Wide
};

std::ostream& operator<<(std::ostream& os, ViewWidth width);

enum class ViewQuality {
    Low,
    High
};

std::ostream& operator<<(std::ostream& os, ViewQuality quality);

enum class AttentionToTargetSide {
    Left,
    Right,
    Our,
    Opp
};

std::ostream& operator<<(std::ostream& os, AttentionToTargetSide side);


struct AttentionToTarget {
    AttentionToTargetSide side;
    int uniformNumber;
};

class AttentionToCommand {
public:
    AttentionToCommand() = default;
    AttentionToCommand(const AttentionToCommand&) = delete;
    AttentionToCommand(AttentionToCommand&&) = default;
    void turnOff() noexcept {
        target.reset();
    }
    void turnOn(std::unique_ptr<AttentionToTarget>&& newTarget) noexcept {
        target = std::forward<std::unique_ptr<AttentionToTarget>>(newTarget);
    }
    bool isTurnedOn() const noexcept { return static_cast<bool>(target); }
    std::optional<AttentionToTarget> getTarget() const noexcept;
private:
    std::unique_ptr<AttentionToTarget> target;
};


struct PointToTarget {
    double distance;
    double direction;
};

class PointToCommand {
public:
    PointToCommand() = default;
    PointToCommand(const PointToCommand&) = delete;
    PointToCommand(PointToCommand&&) = default;
    void turnOff() noexcept {
        target.reset();
    }
    void turnOn(std::unique_ptr<PointToTarget>&& newTarget) noexcept {
        target = std::forward<std::unique_ptr<PointToTarget>>(newTarget);
    }
    bool isTurnedOn() const noexcept { return static_cast<bool>(target); }
    std::optional<PointToTarget> getTarget() const noexcept;
private:
    std::unique_ptr<PointToTarget> target;
};


enum class EarCommandAction {
    Off,
    On
};

std::ostream& operator<<(std::ostream& os, EarCommandAction side);

enum class EarCommandEnumTarget {
    Both,
    Our,
    Opp,
    Left,
    Right
};

std::ostream& operator<<(std::ostream& os, EarCommandEnumTarget side);

enum class EarCommandType {
    CompleteAndPartial,
    Complete,
    Partial
};

std::ostream& operator<<(std::ostream& os, EarCommandType side);

class EarCommandParams {
public:
    EarCommandParams()
    : target(EarCommandEnumTarget::Both)
    , type(EarCommandType::CompleteAndPartial)
    {}
    
    void setTarget(EarCommandEnumTarget newTarget) noexcept {
        target = std::move(newTarget);
    }
    void setTarget(std::string teamName) noexcept {
        target = std::move(teamName);
    }
    bool hasEnumTarget() const noexcept {
        return std::holds_alternative<EarCommandEnumTarget>(target);
    }
    bool hasTeamNameTarget() const noexcept {
        return std::holds_alternative<std::string>(target);
    }
    std::optional<EarCommandEnumTarget> getEnumTarget() const noexcept;
    std::optional<std::string> getTeamName() const noexcept;

    void setType(EarCommandType newType) noexcept {
        type = std::move(newType);
    }
    EarCommandType getType() const noexcept {
        return type;
    }
private:
    std::variant<EarCommandEnumTarget, std::string> target;
    EarCommandType type;
};

class EarCommand {
public:
    EarCommand() = default;
    EarCommand(const EarCommand&) = delete;
    EarCommand(EarCommand&&) = default;
    void setAction(EarCommandAction newAction) noexcept {
        action = std::move(newAction);
    }
    EarCommandAction getAction() const noexcept {
        return action;
    }
    void setParams(std::unique_ptr<EarCommandParams>&& newParams) noexcept {
        parameters = std::forward<std::unique_ptr<EarCommandParams>>(newParams);
    }
    bool hasParams() const noexcept { return static_cast<bool>(parameters); }
    std::optional<EarCommandParams> getParams() const noexcept;
private:
    EarCommandAction action;
    std::unique_ptr<EarCommandParams> parameters;
};
